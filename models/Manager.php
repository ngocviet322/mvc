<?php
namespace models;
use libraries\Model;
class Manager extends Model{


	public function __construct(){
		parent::__construct();
	}

	public function getManagerId(){
		return $managerId;
	}
	
	public function setManagerId($managerId){
		$this->managerId=$managerId;
	}

	public function doLogin($username,$userpass){
		try{
		
		$sql  ="SELECT * FROM Manager WHERE username=:username AND userpass=:userpass";
		$stmt =$this->con->prepare($sql);
		
		$stmt->bindParam(":username",$username);
		$stmt->bindParam(":userpass",$userpass);
		
		$stmt->execute();
		
		$row  =$stmt->fetch();

		if($row){
			session_start();
			$_SESSION['id']           =$row['manager_id'];
			$_SESSION['user_name']    =$row['username'];
			$_SESSION['access_level'] =$row['access_level'];
			header("location:../dashboard");
		}else{
			header("location:../login");
		}
		}catch(PDOException $e){
			echo $e->getMessage();

		}
		$this->con=NULL;
	}

	public function getRecord($id){

		try{
			$sql  ="SELECT * FROM Manager WHERE manager_id=:id";
			$stmt =$this->con->prepare($sql);

			$stmt->bindParam(":id",$id);

			$stmt->execute();
			$row=$stmt->rowCount();

			if($row > 0){
				$this->con=NULL;
				return $stmt->fetchAll(\PDO::FETCH_ASSOC);	
			}
			
		}catch(PDOException $e){
			echo $e->getMessage();
			$this->con=NULL;
		}

	}
}


?>