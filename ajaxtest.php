<?php
require 'config/general.php';
	$id=$_POST['id'];
		try{
			$strCon="mysql:host=". HOST .";dbname=". DB_NAME .";charset=utf8";
			$con=new \PDO($strCon,DB_USER,DB_PASS);
			$con->setAttribute(\PDO::ATTR_ERRMODE,\PDO::ERRMODE_EXCEPTION);
			$con->setAttribute( \PDO::ATTR_EMULATE_PREPARES, false );
			$sql  ="DELETE FROM Manager WHERE manager_id=:id";
			$stmt =$con->prepare($sql);

			$stmt->bindParam(":id",$id);

			$bool=$stmt->execute();
			$con=NULL;
			echo $bool;
		}catch(PDOException $e){
			echo $e->getMessage();
			$con=NULL;
		}

?>