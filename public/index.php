<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Test HTML-CSS Ngô Ngọc Việt</title>
	<link rel="stylesheet" type="text/css" href="public/styles/mystyle.css">
	<link rel="stylesheet" type="text/css" href="public/styles/reset.css">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script type="text/javascript" src="public/javascript/myjs.js"></script>
</head>
<body>
	<a name="top" href="#"> </a>
	<a id="btop" href="#top"><img src="public/images/top.png" alt="back to top"></a>
	<div class="top-header">
		<header class="main-header">
			<h1>Continuum<h1>
		</header>
		<nav class="main-nav">
			<ul>
				<li><span>Shortcodes</span></li>
				<li><span>Full Width</span></li>
				<li><span>Contract</span></li>
				<li><span>About</span></li>
				<li class="nav-active"><span>Home</span></li>
			</ul>
		</nav><div class="clear"></div>
	</div>
	<div class="slider" id="slider">
		<ul class="slides">
			<li class="slide"><img src="public/images/column1.png" alt="Slide 1"></li>
			<li class="slide"><img src="public/images/column2.png" alt="Slide 2"></li>
			<li class="slide"><img src="public/images/column3.png" alt="Slide 3"></li>
			<li class="slide"><img src="public/images/column4.png" alt="Slide 4"></li>
		</ul>
	</div>
	<hr>
	<div class="content" id="content">
		<div class="portfolio">
			<p class="title-pro">Sed portifolio ipsum at justo  suscipit quis sollicitudin dolor facilisis. Fusce vestibulum blanndict ultricies</p>
			<div class="viewpro">
				<a href="#">View Porfolio</a>
			</div><div class="clear"></div>
		</div>

		<section class="main-content" id="main-content">
	<!-- 		<article class="art-detail">
				<div class="art-img" >
					<img src="images/colum1.png" alt="colum1">
				</div>
				<header class="art-header"><h1><a href="#">Fusce volutpat elementum</a></h1></header>
				<div class="art-content">
					<p>Maecenas arcu lectus, vehicula vitae dignissim id, venenatis convallis urma.In viate lacus et susto congue scelerisque. Duis purus neque ,ultricses sed vulputae quis, vehicula at lorem. praesent lobortis posurere ticidunt</p>
				</div>
			</article>

			<article class="art-detail">
				<div class="art-img" >
					<img src="images/column2.png" alt="colum2">
				</div>
				<header class="art-header"><h1><a href="#">Fusce volutpat elementum</a></h1></header>
				<div class="art-content">
					<p>Maecenas arcu lectus, vehicula vitae dignissim id, venenatis convallis urma.In viate lacus et susto congue scelerisque. Duis purus neque ,ultricses sed vulputae quis, vehicula at lorem. praesent lobortis posurere ticidunt .</p>
				</div>
			</article>

			<article class="art-detail">
				<div class="art-img" >
					<img src="images/column3.png" alt="colum3">
				</div>
				<header class="art-header"><h1><a href="#">Fusce volutpat elementum</a></h1></header>
				<div class="art-content">
					<p>Maecenas arcu lectus, vehicula vitae dignissim id, venenatis convallis urma.In viate lacus et susto congue scelerisque. Duis purus neque ,ultricses sed vulputae quis, vehicula at lorem. praesent lobortis posurere ticidunt</p>
				</div>
			</article>
					<article class="art-detail">
				<div class="art-img" >
					<img src="images/column3.png" alt="colum3">
				</div>
				<header class="art-header"><h1><a href="#">Fusce volutpat elementum</a></h1></header>
				<div class="art-content">
					<p>Maecenas arcu lectus, vehicula vitae dignissim id, venenatis convallis urma.In viate lacus et susto congue scelerisque. Duis purus neque ,ultricses sed vulputae quis, vehicula at lorem. praesent lobortis posurere ticidunt</p>
				</div>
			</article> -->
			<!-- <div class="clear"></div> -->
		</section>
	</div>
	<footer class="main-footer">
		<article class="art-footer">
			<header class="art-footer-header"><h1><a href="#">Recents Post</a></h1></header>
			<ul>
				<li><a href="#">Nulla nulla nisl faucibus vitae pulviar</a></li>
				<li><a href="#">Suspendisse leo tortor, sollicitudin</a></li>
				<li><a href="#">Pellentesque dolor urma,mollis sed.</a></li>
				<li><a href="#">Aliquam erat volutpat. Set quis elit.</a></li>
			</ul>
			
		</article>

		<article class="art-footer">
			<header class="art-footer-header"><h1><a href="#">Categorys</a></h1></header>
			<ul>
				<li><a href="#">Website Development</a></li>
				<li><a href="#">Portfolio</a></li>
				<li><a href="#">Technology News</a></li>
				<li><a href="#">World Economoics</a></li>
			</ul>
			
		</article>

		<article class="art-footer">
			<header class="art-footer-header"><h1><a href="#">Pages</a></h1></header>
			<ul>
				<li><a href="#">Home</a></li>
				<li><a href="#">About Us</a></li>
				<li><a href="#">Contract Our Team</a></li>
				<li><a href="#">Shortcodes</a></li>
			</ul>
			
		</article>

		<article class="newsletter">
			<header class="art-newsletter-header"><h1><a href="#">Newsletter</a></h1></header>
			<p>Subcribe to our newsletter and get exclusive deals you won't find anywhere else straight to your inbox</p>
			<form id="sub" method="POST" action="#" onsubmit="return;" >
				<div class="form-wrapper">
					<input type="email" required>
					<button type="submit">Subscribe</button>
				</div>
			</form>		
			<p id="demo" class="demo"></p>
		</article><div class="clear"></div>
		<!-- <button onclick="">Load Ajax</button> -->
		<p id="demojson" onclick="jsonTest();">11111</p>
	</footer>

	<div class="social">
		<ul>
			<li style="margin-bottom: 5px"><a href="#"><img src="public/images/googlered.png" alt="google"></a></li>
			<li style="margin-bottom: 5px"><a href="#"><img src="public/images/facebook.png" alt="facebook"></a></li>
			<li style="margin-bottom: 5px"><a href="#"><img src="public/images/twitter.png" alt="twitter"></a></li>
		</ul>
	</div>
</body>
</html>