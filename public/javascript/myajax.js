$(document).ready(function(){

	$("#image").live('change',function() {
       var data;
       data=new FormData();
       data.append('title',$('#title').val());
       data.append('filename',this.files[0]);
       $.ajax({
	       	url:'http://localhost/MVC/manager/uploadImage',
	        data: data,
	        processData: false,
	        contentType: false,
	        type: 'POST',
	        error: function (xhr, ajaxOptions, thrownError) {
		        alert(xhr.responseText);
		        alert(thrownError);
		    },
	        xhr: function () {
		        var xhr = new window.XMLHttpRequest();
		        //Download progress
		        xhr.addEventListener("progress", function (evt) {
		            console.log(evt.lengthComputable);
		            if (evt.lengthComputable) {
		                var percentComplete = evt.loaded / evt.total;
		                // $('#progress').html('Uploading'+Math.round(percentComplete * 100) + "%");
		                $('#progressbar').width(Math.round(percentComplete * 100)+'%');
		            }
		        }, false);
		        return xhr;
		    },
	        beforeSend: function (request)
            {
               	$('#progressbox').show();
            },
            complete:function(){
            	$('.textStatus').html('Upload Completed');
            },
	        success: function ( data ) {
	            // if(data==1){
	            // 	console.log("ok");
	            // }else{
	            // 	console.log('fail');
	            // }
	            console.log(data);
	        }
       	});
    });

});


function deleteUser(ele,id){
		// alert(ele.value+"ajaxtest.php");
	$.ajax({
		url:ele.value+"/manager/deleteUser",
		type:"POST",
		data:{'id':id},
		success:function(data){
			if(data==1){
				location.reload();
			}
		},
		error: function (XMLHttpRequest, textStatus, errorThrown) {
   			 console.log('XHR ERROR ' + XMLHttpRequest.status + "-" + XMLHttpRequest.responseText);
		}
	});
	return false;
}
function getRecord(ele,id){
	$.ajax({
		url:ele.value+"/manager/getRecord",
		type:"POST",
		data:{'id':id},
		dataType: "json",
		success:function(data){
			console.log(data[0].username);
		},
		error: function (XMLHttpRequest, textStatus, errorThrown) {
   			 console.log('XHR ERROR ' + XMLHttpRequest.status + "-" + XMLHttpRequest.responseText);
		}
	});
	return false;
}

function checkPasswordMatch() {
    var password = $("#pass").val();
    var confirmPassword = $("#repass").val();
    if (password != confirmPassword){
        $("#repass").css({"box-shadow":"0 0 5px #d45252","border-color":"#b03535","background":"#fff url(images/invalid.png) no-repeat 98% center"});
    	$("#repass").focus();
    }
    else
        $("#repass").css({"box-shadow":"0 0 5px #5cd053","border-color":"#28921f","background":"#fff url(images/valid.png) no-repeat 98% center"});
}