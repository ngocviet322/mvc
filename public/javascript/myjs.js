$(document).ready(function(){
	var slideWidth=$(window).width()*80/100;
	$(".slide").width(slideWidth);
	// Display Back Top Image 
	var displayBTop=$(".slider").offset().top;
	$(window).scroll(function(){
		if($(this).scrollTop()>displayBTop){
			$("#btop").fadeIn("slow");
		}else{
			$("#btop").fadeOut("slow");
		}
	});
	$.ajax({
		url: 'http://localhost/MVC/index/loadArticle',
		type:'GET',
	    dataType: "json",
	    success: function (data) {   	
			$("#main-content").append(displayArticle(data));
	    }
	});
	// Hide Border active li when enter child li
	$(".main-nav").on({
		mouseenter:function(){

			console.log($(this).css("borderColor"));
			if(!$(this).is(":last-child")){
				$(".nav-active").css({
					"border-color":"#fff"
				});
			}
		},
		
		mouseleave:function(){
			$(".nav-active").css({
				"border-color":"#d8d8d8"
			});
		},

		click:function(){

				$(this).css({
					"border":"1px solid #d8d8d8"
				});
				$(this).siblings().css({
					"border-color":"#fff"
				});
		
				if(!$(this).is(".nav-active")){
					$(".nav-active").css({
						"border-top":"0",
						"border-left":"0",
						"border-right":"0"
					});		
				}
				
		}

	},"li");
	
	// Read Json 



	// $.ajax({
	// 		url: 'data/acticles.json',
	//       	dataType: "json",
	//       	success: function (data) {   	
	// 			$("#main-content").append(displayArticle(data));
	//       	}
	// });

		// $.ajax({
		// 	url: 'data/acticles.json',
	 //      	dataType: "json",
	 //      	success: function (data) {
	 //        	// alert(data.links.next.href);
	 //      	}
		// });
	// Cached DOM
	var slider=$(".slider");
	var slideContainer=slider.find(".slides");
	var slides=slideContainer.find(".slide");

	// Params 
	var width=slides.width();
	var speed=1000;
	var pause=6000;
	var currentSlide=1;
	var myInterval;
	var called=false;
	function startSlide(){
			myInterval=setInterval(function(){
				// console.log(slideContainer.css("marginLeft") + currentSlide);
				slideContainer.animate({"margin-left":"-="+width},speed,function(){
					currentSlide++;
					// console.log(slideContainer.css("marginLeft") + currentSlide);

					if(currentSlide === slides.length){
						currentSlide = 1;
						slideContainer.delay(3000).animate({"margin-left":0},1000);
						// stopSlide();
					}
				});
			},pause);
		}
	function stopSlide(){
		clearInterval(myInterval);
	}
	// alert(width);
	startSlide();
	slideContainer.on({
		mouseenter:function(){
			stopSlide();
			// console.log("Stop Slide");
		},
		mouseleave:function(){
			startSlide();
			// console.log("Start Slide");
		}
	});

});

function viewMore(){
	console.log($('.art-detail').length);
	$.ajax({
		url: 'http://localhost/MVC/index/loadArticle/'+$('.art-detail').length,
		type:'GET',
	    dataType: "json",
	    beforeSend: function (request)
        {
        },
		complete:function(){
           	$('#viewmore').remove();
           	$('#main-content').append("<div class='clear'></div>");
        },
	    success: function (data) {
	    	if(data === null){
				$('#viewmore').css({'display':'none'});
				return false;
			}
	        var str="";
			$("#viewmore").before(displayArticle(data));
	    }
	});
}

function displayArticle(data){
	var str="";
	var i=1;
	for (var obj in data) {
		str+="<article class='art-detail'>"+
		"<div class='art-img'>"+
		"<img src='public/uploads/"+data[obj].manager_id+"/"+data[obj].image+"' alt='"+data[obj].image+"'></div>"+
		"<header class='art-header'><h1><a href='#'>"+data[obj].title+"</a></h1></header>"+
		"<div class='art-content'>"+
		"<p>"+ data[obj].intro+"</p>"+
		"</div></article>";
		if(i == 3){
			str+="<button id='viewmore' style='width:100%;height:50px;margin-bottom:10px;' onclick='viewMore();'>View More</button>";
			break;
		}
		if(i==data.length){
		str+="<div class='clear'></div>";
		}
		i++;
	}
	return str;   
}
