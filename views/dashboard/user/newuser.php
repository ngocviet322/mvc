 <section id="main" class="column">
    
    <h4 class="alert_info">Welcome to the free MediaLoot admin panel template, this could be an informative message.</h4>
        <div class="clear"></div>
      </div>
    </article><!-- end of stats article -->
    <article class="module width_full">
    <header>
      <h3>Thông Tin</h3>
    </header>
    <div class="module_content">
      <p class="displayName" style="display: none;"></p>
      <article class="stats_graph">
        <form method="POST"  class="info_form" id="info_form" name="info_form" action="<?=URL?>manager/add">
          <ul>
            <li><label for="idname">Tên Đăng Nhập</label> <input
              type="text" id="idname" name="username" required /> <span
              class="form_hint">Proper format "Aa-Zz"</span>
            </li>
            <li><label for="pass">Mật Khẩu :</label> <input
              type="password" id="pass" name="userpass" required /> <span
              class="form_hint">Proper format "Aa-Zz"</span>
            </li>
            <li><label for="repass">Nhập Lại Mật Khẩu :</label> <input
              type="password" id="repass" name="reuserpass" onchange="checkPasswordMatch();" required /> <span
              class="form_hint">Proper format "Aa-Zz"</span>
            </li>
            <li><label for="level">Quyền Hạn :</label>
               <select name="level">
                <option value="Admin">Admin</option>
                <option value="Mod">Mod</option>
              </select>
            </li>
            <li>
              <button class="submit" type="submit">Submit</button>
            </li>
          </ul>
        </form>
      </article>
      <div class="clear"></div>
    </div>
  </article>
    <div class="clear"></div>  
    <div class="spacer"></div>
  </section>