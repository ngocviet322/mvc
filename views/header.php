<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8"  http-equiv="Cache-control" content="public">
	<title>Ngô Ngọc Việt</title>
	<link rel="stylesheet" type="text/css" href="<?=URL?>public/styles/mystyle.css">
	<link rel="stylesheet" type="text/css" href="<?=URL?>public/styles/reset.css">
	<script  src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js" ></script>
	<script   type="text/javascript"  src="<?=URL?>public/javascript/myjs.js" async></script>


</head>
<body>
	<a name="top" href="#"> </a>
	<a id="btop" href="#top"><img src="public/images/top.png" alt="back to top"></a>
	<div class="top-header">
		<header class="main-header">
			<h1>Continuum<h1>
		</header>
		<nav class="main-nav">
			<ul>
				<li><a href="<?=URL?>login/init"><span>Login</span></a></li>
				<li><span>Full Width</span></li>
				<li><span>Contract</span></li>
				<li><span>About</span></li>
				<li class="nav-active"><a href="<?=URL?>index"><span>Home</span></a></li>
			</ul>
		</nav><div class="clear"></div>
	</div>
	<div class="slider" id="slider">
		<ul class="slides">
			<li class="slide"><img src="<?=URL?>/public/images/column1.png" alt="Slide 1"></li>
			<li class="slide"><img src="<?=URL?>/public/images/column2.png" alt="Slide 2"></li>
			<li class="slide"><img src="<?=URL?>/public/images/column3.png" alt="Slide 3"></li>
			<li class="slide"><img src="<?=URL?>/public/images/column4.png" alt="Slide 4"></li>
		</ul>
	</div>
	<hr>