<?php
use libraries\Controller;
use models\Manager as Model_Manager;
use controllers\Error;

class Manager extends Controller{
	private $managerId;
	public function __construct(){
		parent::__construct();

	}



	public function deleteUser(){
		$id=$_POST['id'];

		$param=array('manager_id'=>$id);
		if(!$this->checkIsset($param)){
			$error=new Error();
			$error->init();	
		}

		$model=new Model_Manager();
		$numRow=$model->deteleRecord($param);

		echo $numRow;
	}

	public function getRecord(){
		$id=$_POST['id'];
		$param=array('manager_id'=>$id);

		if(!$this->checkIsset($param)){
			$error=new Error();
			$error->init();
		}

		$model=new Model_Manager();
		$arr=$model->getRecord($id);
		echo json_encode($arr);
	}

	public function add(){
		$username=$_POST['username'];
		$userpass=$_POST['userpass'];
		$reUserpass=$_POST['reuserpass'];
		$level=$_POST['level'];
		$userpass=$this->encodeMD5($userpass);
		
		$params=array(
			'username'=>$username,
			'userpass'=>$userpass,
			'access_level'=>$level
			);

		if(!$this->checkIsset($params) || $userpass!=$reUserpass){
			$error=new Error();
			$error->init();
		}

		$model=new Model_Manager();
		$this->view->count=$model->addRecord($params);
		header('location:'.URL.'dashboard/viewUser');
	}

	public function uploadImage(){
		echo parent::uploadImage('filename');
	}
}


?>