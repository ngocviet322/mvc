<?php
/**
* @author Ngoc Viet
*/
namespace controllers;
use libraries\Controller;
use models\Article;
class Index extends Controller
{
	
	public function __construct()
	{
		Parent::__construct();
		// $this->view->render("help/index",0);
	}

	public function init(){
		$arti=new Article();
		$params=array(':start' => 0 , ':display' => 6 ,':sortby'=>'article_id');
		$this->view->arti=$arti->getRecords($params);
		$this->view->render("index/index");
	}

	public function loadArticle($start=NULL){
		$arti=new Article();
		if($start==NULL){
			$params=array(':start' => 0 , ':display' => 3 ,':sortby'=>'article_id');
		}else{
			$params=array(':start' => $start , ':display' => 3 ,':sortby'=>'article_id');
		}

		echo json_encode($arti->getRecords($params));
	}

	public function details(){
		$this->view->render("index/index");
	}

}



?>