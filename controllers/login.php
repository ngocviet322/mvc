<?php

use libraries\Controller;
use models\Manager;

class Login extends Controller{
	private $model;
	public function __construct(){
		parent::__construct();
		// $this->view->render("login/index",1);
	}

	public function init(){
		$this->view->render("login/index",1);
	}

	public function dologin(){
		$username=$_POST['username'];
		$userpass=$_POST['userpass'];
		$userpass=$this->encodeMD5($userpass);
		if(isset($username) && isset($userpass)){
			$this->view->model=new Manager();
			$this->view->model->doLogin($username,$userpass);
		}else{
			throw new Exception("Error Processing Request", 1);
		}

	}

	

	
}

?>